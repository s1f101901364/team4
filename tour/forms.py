from django import forms#フォームライブラリ
from .models import Contact,Category # Metaを使用?????





# お問い合わせのform
class Contact_Form(forms.ModelForm):

    class Meta():
        #モデルクラスを指定
        model = Contact

        #表示するモデルクラスのフィールドを定義
        fields = ('name','title', 'mail','content')

        #表示ラベルを定義
        labels = {'name':"名前",
                  'title':"タイトル",
                  'mail':"メール",
                  'content':"内容",
            }




# 新規投稿ページその1

class PostForm(forms.Form):
    def __init__(self, *args, **kwargs):  # modelform???????????????????????????
        super().__init__(*args, **kwargs)
        # カテゴリーを取得し、選択肢を作成
        category_data = Category.objects.all()
        category_choice = {}
        for category in category_data:
            category_choice[category] = category
        self.fields["category"].choices = list(category_choice.items())
    title = forms.CharField(max_length=200, label='タイトル')
    address = forms.CharField(max_length=200, label='住所', required=False)
    # フォームで登録されたカテゴリーを選択できる
    category = forms.MultipleChoiceField(label='カテゴリ', widget=forms.CheckboxSelectMultiple, required=False)
    url = forms.CharField(max_length=500, label='URL', required=False)
    content = forms.CharField(label='内容', widget=forms.Textarea())
    # required=False => 必須ではなくても良い
    image = forms.ImageField(label='イメージ画像', required=False)


# 新規投稿ページその1

#class PostForm(forms.Form):
#    def __init__(self, *args, **kwargs): # modelform???????????????????????????
#        super().__init__(*args, **kwargs)
        # カテゴリーを取得し、選択肢を作成
#        category_data = Category.objects.all()
#        category_choice = {}
#        for category in category_data:
#            category_choice[category] = category
#        self.fields["category"].choices = list(category_choice.items())
#    title = forms.CharField(max_length=200, label='タイトル')
#    adress = forms.CharField(max_length=200, label='住所',required=False)
    # フォームで登録されたカテゴリーを選択できる
#    category = forms.ChoiceField(label='カテゴリ', widget=forms.CheckboxSelectMultiple, required=False)
#    url =  forms.CharField(max_length=500, label='URL', required=False)
#    content = forms.CharField(label='内容', widget=forms.Textarea())
#    image = forms.ImageField(label='イメージ画像', required=False) # required=False => 必須ではなくても良い








#class PostForm(forms.Form):
#    category_data = Category.objects.all() #全ての投稿にカテゴリーを付けるように設定
#    category_choice = {} # 空の辞書を作成???
#    for category in category_data:
#        category_choice[category] = category #カテゴリーの辞書を作成

#    title = forms.CharField(max_length=30, label='タイトル')
    # フォームで登録されたカテゴリーを選択できる
#    category = forms.ChoiceField(label='カテゴリ', widget=forms.Select, choices=list(category_choice.items()))
#    content = forms.CharField(label='内容', widget=forms.Textarea())
#    image = forms.ImageField(label='イメージ画像', required=False) # required=False => 必須ではなくても良い












