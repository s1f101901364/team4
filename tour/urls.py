from django.urls import path
from tour import views

urlpatterns = [
    path('', views.IndexView.as_view(), name='index'),# if last URL is  " " => go to "views.py"
    path('FAQ/', views.FAQ, name='FAQ'), # URL "~~/faq"  => FAQ先に遷移
    path('contact/', views.FormView.as_view(), name='contact'), # "~~~/contact"  => 問い合わせ先に遷移  
    path('post/<int:pk>/', views.post, name='post_detail'), #投稿の詳細
    path('post/new/', views.CreatePostView.as_view(),name='post_new'), #投稿
    path('post/<int:pk>/edit/', views.PostEditView.as_view(),name='post_edit'),#投稿の編集
    path('post/<int:pk>/delete/', views.PostDeleteView.as_view(), name='post_delete'),
    path('category/<str:category>', views.CategoryView.as_view(), name='category'), # <str:category> => カテゴリの名前がURL => category/apple
    path('search', views.SearchView.as_view(),name='search'), # <str:category> => カテゴリの名前がURL => category/apple
    path('all/', views.AllView.as_view() ,name='all'), # 全部の投稿リストを閲覧
    path('post/<int:pk>/like',views.like,name="like"),
    path("api/like/<int:pk>/", views.api_like, name="api_like"),
    path('NewUser/',views.NewUser,name = 'NewUser'),#初めての人
    path('create_accounts/', views.create_account, name='create_account'),#アカウント作成

]