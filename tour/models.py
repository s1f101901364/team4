from django.db import models


import datetime


# 投稿用
from django.conf import settings
from django.utils import timezone

# マイグレーションコード
# python manage.py makemigrations
# python manage.py migrate


# Create your models here.


class Value(models.Model):
    title = models.CharField(max_length=256)
    detail = models.TextField(default='詳細')
    create = models.DateTimeField(default=datetime.datetime.now)
    like = models.IntegerField(default=0)


# お問い合わせ
# モデルクラスを定義
class Contact(models.Model):
    name = models.CharField(max_length=100)
    title = models.CharField(max_length=100)
    mail = models.EmailField(max_length=100)

    content = models.TextField()

# カテゴリーモデルを追加


class Category(models.Model):
    name = models.CharField('カテゴリー', max_length=100)

    # バックの/admin でモデルを見る際にnameを表示
    def __str__(self):
        return self.name


class Post(models.Model):
    # 筆者のカラム作成
    # foreignkey => 他のモデルと連携 => ログインユーザと連携(settings.AUTH_USER_MODEL), 一つ消えたら全部消える(on_delete=models.CASCADE)
    author = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    # カテゴリーとPostを連携, => on_delete=models.PROTECT
    # on_delete=models.PROTECT) => カテゴリーを削除した時に、投稿のモデルに影響を与えない
    category = models.ManyToManyField(Category, verbose_name='カテゴリ')  # カテゴリーを消しても投稿データは残る
    title = models.CharField("タイトル", max_length=200)
    address = models.CharField("住所", max_length=200, blank=True, null=True)
    url = models.URLField("URL", max_length=500, blank=True, null=True)

    # 画像の高さと幅を自動調節???
    # image_height = models.PositiveIntegerField(editable=False)
    # image_width = models.PositiveIntegerField(editable=False)
    # image_width = models.IntegerField()
    # image_height = models.IntegerField()
    # upload_toで画像のアップロード先を指定

    image = models.ImageField(upload_to='images', max_length=100,
                              verbose_name='イメージ画像', null=True, blank=True)  # 追加
    content = models.TextField("本文")
    created = models.DateTimeField("作成日", default=timezone.now)
    like = models.IntegerField(default=0)#いいね機能
    # djangoの管理画面でタイトルだけ見えるようにする
    def __str__(self):
        return self.title

# commnet

from django.contrib.auth.models import User

class Comments(models.Model):
    text = models.TextField()
    create = models.DateTimeField('date published')
    comment = models.ForeignKey(Post, related_name='comments', on_delete=models.CASCADE)
    #username = models.ForeignKey(User,on_delete=models.CASCADE)
