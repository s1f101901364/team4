from .models import Category

# すべてのカテゴリ―データをどのテンプレートでも取得できるようになる
def common(request):
    category_data = Category.objects.all()
    context = {
        'category_data': category_data
    }
    return context

#def common1(request):
#    category_data = Category.objects.all()
#    context = {
#        'category_data': category_data
#    }
#    return context    