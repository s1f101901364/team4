from django.shortcuts import render, redirect
# from django.contrib.auth.decorators import login_required
import datetime

from .models import Value, Comments, Post, Category
from django.http import Http404

from . import forms

# おとい合わせ
from django.views.generic import TemplateView

# 投稿用
from django.contrib.auth.mixins import LoginRequiredMixin  # ログインが必須になる
from .forms import PostForm  # 新規投稿
from django.views.generic import View  # classの汎用であるViewをimport
from django.db.models import Q  # or検索
from functools import reduce  # 畳込み演算
from operator import and_, pos  # 足し算

# アカウント作成用
from django.contrib.auth.models import User
from django.contrib.auth import login, authenticate
from django.views.generic import CreateView
from accounts.form import UserCreateForm


# Create your views here.

def miru(request):  # views.home
    return render(request, 'tour/miru.html')  # go to tour/miru.html

def NewUser(request):  # views.home
    return render(request, 'tour/NewUser.html')  # go to tour/NewUser.html

def FAQ(request):  # views.home
    return render(request, 'tour/FAQ.html', {})  # go to tour/FAQ.html

# アカウント作成
class Create_account(CreateView):
    def post(self, request, *args, **kwargs):
        form = UserCreateForm(data=request.POST)
        if form.is_valid():
            form.save()
            # フォームから'username'を読み取る
            username = form.cleaned_data.get('username')
            # フォームから'password1'を読み取る
            password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=password)
            login(request, user)
            return redirect('/')
        return render(request, 'tour/create_account.html', {'form': form, })

    def get(self, request, *args, **kwargs):
        form = UserCreateForm(request.POST)
        return render(request, 'tour/create_account.html', {'form': form, })

create_account = Create_account.as_view()


# お問い合わせの views,py
class FormView(TemplateView):

    # 初期変数定義
    def __init__(self):
        self.params = {"Message": "情報を入力してください。",
                       "form": forms.Contact_Form(),
                       }

    # GET時の処理を記載
    def get(self, request):
        return render(request, "tour/contact.html", context=self.params)

    # POST時の処理を記載
    def post(self, request):
        if request.method == "POST":
            self.params["form"] = forms.Contact_Form(request.POST)

            # フォーム入力が有効な場合
            if self.params["form"].is_valid():
                # 入力項目をデータベースに保存
                self.params["form"].save(commit=True)
                self.params["Message"] = "入力情報が送信されました。"
                return render(request, "tour/contact.html", context=self.params)


class IndexView(View):
    def get(self, request, *args, **kwargs):  # viewが呼ばれたら最初の処理をする
        # Postモデルからデータを取得
        post_data = Post.objects.order_by('-id')  # 降順にデータを取得
        return render(request, 'tour/index.html', {  # 指定したhtmlにデータを渡す
            'post_data': post_data  # データを辞書型
        })


# 投稿のの詳細
class PostDetailView(View):
    def get(self, request, *args, **kwargs):
        post_data = Post.objects.get(id=self.kwargs['pk'])  # URLのIDを取得 => 特定のIDを獲得
        #user = User.objects.get(username = request.user.username)
        username = User.objects.get(username = self.request.user)  
        if request.method == 'POST':
            comments = Comments(comment=post_data, text=request.POST['text'], create=datetime.datetime.now())
            comments.save()
 
        return render(request, 'tour/post_detail.html', {
            'post_data': post_data,
            'comments': post_data.comments.order_by('create'),
            'username' : username
        })

    def post(self, request, *args, **kwargs):
        post_data = Post.objects.get(id=self.kwargs['pk'])
        if request.method == 'POST':
            comments = Comments(comment=post_data, text=request.POST['text'], create=datetime.datetime.now())
            comments.save()
            username = User.objects.get(username = self.request.user)
            username.save()
        return render(request, 'tour/post_detail.html', {
            'post_data': post_data,
            'comments': post_data.comments.order_by('create'),
            'username' : username
        })


post = PostDetailView.as_view()

# ログイン者のみ投稿の作成
# LoginRequiredMixin => ログイン者のみに制限


class CreatePostView(LoginRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        form = PostForm(request.POST or None)  # Postformを呼び出す
        return render(request, 'tour/post_form.html', {
            'form': form
        })

    # 投稿ボタンを押したときの動作
    def post(self, request, *args, **kwargs):
        form = PostForm(request.POST or None)
        if form.is_valid():  # formの内容をチェックしたら
            post_data = Post.objects.create(
                author=request.user,
                title=form.cleaned_data['title'],
                address=form.cleaned_data['address'],
                url=form.cleaned_data['url'],
            #    categories = form.cleaned_data['category'],
                content=form.cleaned_data['content'],
            )
            # フォームから入力されたカテゴリーを取得
            categories = form.cleaned_data['category']
            for category in categories:
                category_data = Category.objects.get(name=category)
                post_data.category.add(category_data)

            # フォームから画像を取得する方法は、request.FILESを使用
            if request.FILES:  # 画像ガアップロードされた時
                post_data.image = request.FILES.get('image')  # 画像データを取得
            post_data.save()  # データベースを保存

            return redirect('post_detail', post_data.id)  # 詳細画面にリダイレクト

        # エラーの場合  => 投稿フォームを表示
        return render(request, 'tour/post_form.html', {
            'form': form
        })


# ログイン者のみ投稿の編集可能
class PostEditView(LoginRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        post_data = Post.objects.get(id=self.kwargs['pk'])
        # 初期値を入力しておくからinitalを追加
        form = PostForm(
            request.POST or None,
            # 画像の初期データは、他のデータと同じように指定
            initial={
                'title': post_data.title,  # データベースからtitleを取得
                'category': post_data.category.add,
                'address': post_data.address,
                'url': post_data.url,
                'content': post_data.content,  # データベースからcontentを取得
                'image': post_data.image,  # 追加
            }
        )

        # もう一度投稿フォームにリダイレクト
        return render(request, 'tour/post_form.html', {
            'form': form
        })

    def post(self, request, *args, **kwargs):
        form = PostForm(request.POST or None)

        if form.is_valid():  # formの内容をチェックしたら
            post_data = Post.objects.get(id=self.kwargs['pk'])  # 特定のデータを取得
            post_data.author = request.user  # ログインユーザーを取得
            # form.cleaned_data => formの内容を取得
            post_data.title = form.cleaned_data['title']  # タイトルのみを取得
            post_data.address = form.cleaned_data['address']

            post_data.url = form.cleaned_data['url']
            post_data.content = form.cleaned_data['content']  # 内容のみを取得

            # フォームから画像を取得する方法は、request.FILESを使用
            if request.FILES:  # 画像ガアップロードされた時
                post_data.image = request.FILES.get('image')  # 画像データを取得
            post_data.save()  # データベースを保存

            # フォームから入力されたカテゴリーを取得（saveしてから追加しないとエラー出る）
            categories = form.cleaned_data['category']
            for category in categories:
                category_data = Category.objects.get(name=category)
                post_data.category.add(category_data)
            post_data.save()
            # return で詳細画面にリダイレクト
            return redirect('post_detail', self.kwargs['pk'])

        return render(request, 'tour/post_form.html', {
            'form': form
        })


# ログイン者のみ投稿を削除可能
class PostDeleteView(LoginRequiredMixin, View):
    def get(self, request, *args, **kwargs):  # viewが呼ばれたら最初の処理をする
        # Postモデルからデータを取得
        post_data = Post.objects.get(id=self.kwargs['pk'])  # 特定のデータを取得
        return render(request, 'tour/post_delete.html', {  # 指定したhtmlにデータを渡す
            'post_data': post_data  # データを辞書型
        })

    # 削除を押したときの動作
    def post(self, request, *args, **kwargs):
        post_data = Post.objects.get(id=self.kwargs['pk'])  # 特定のデータ取得
        post_data.delete()  # そのデータを削除
        # 削除した後
        return redirect('index')  # create.htmlにリダイレクト

# CategoryViewを作成


class CategoryView(View):
    def get(self, request, *args, **kwargs):
        # URLからカテゴリー名を取得して、カテゴリーモデルでフィルターをかけてデータを取得
        category_data = Category.objects.get(name=self.kwargs['category'])
        # order_by関数を使用してデータの並び替えをする => －id => 降順に並べる
        # カテゴリーデータでフィルターをかけます
        post_data = Post.objects.order_by('-id').filter(category=category_data)
        return render(request, 'tour/filter.html', {
            'category': category_data,
            'post_data': post_data
        })


class SearchView(View):
    # この処理内容は、与えられた文言がデータベース内のデータと１つでも一致していれば取得するという処理
    def get(self, request, *args, **kwargs):
        post_data = Post.objects.order_by('-id')  # 降順にデータを取得
        keyword = request.GET.get('keyword')  # 検索ワードからキーワードを取得

        # (2)キーワードをリスト化させる(複数指定の場合に対応させるため)
        if keyword:
            exclusion_list = set(['', '　'])  # 半角と全角を除外する
            query_list = ''
            for word in keyword:
                if not word in exclusion_list:
                    query_list += word  # スペースを除いた文字を取得
            # Qオブジェクトを使用して、投稿データをキーワードでフィルタする
            # キーワードQオブジェクトでOR検索する
            # 条件Aと条件Bのいずれかが満たされる場合（OR条件）」
            # contains, icontains => 部分一致
            query = reduce(and_, [Q(title__icontains=q) | Q(
                content__icontains=q) for q in query_list])

            post_data = post_data.filter(query)  # 投稿データにキーワードでフィルタをかける

        return render(request, 'tour/filter.html', {
            # テンプレートに渡す
            'keyword': keyword,
            'post_data': post_data
        })


# retrive  all data
# def all(request):
#    if request.method == 'GET':
#        Post_data = Post.objects.order_by('-id')
#        #Post_data = Post.objects.all()
#    return render(request, 'tour/all_data.html',{

#        'Post_data': Post_data

#    })


# CategoryViewを作成
class AllView(View):
    def get(self, request, *args, **kwargs):
        # order_by関数を使用してデータの並び替えをする => －id => 降順に並べる
        post_data = Post.objects.order_by('-id')
        # Post_data = Post.objects.all()???????
        return render(request, 'tour/all_data.html', {

            'post_data': post_data

        })


def like(request,pk):
    try:
        post = Post.objects.get(pk=pk)
    except Post.DoesNotExist:
        raise Http404
    post.like += 1 # ここでいいねの数を増やす
    post.save() # 保存をする
    return redirect('post_detail',pk)

def api_like(request,pk):
    try:
        post = Post.objects.get(pk=pk)
    except Post.DoesNotExist:
        raise Http404
    post.like += 1 # ここでいいねの数を増やす
    post.save() # 保存をする