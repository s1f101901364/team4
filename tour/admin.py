from django.contrib import admin
from .models import Post,Contact,Category # 作成したモデルをインポート

# Register your models here.

admin.site.register(Post) # register(Post)でモデルを登録

admin.site.register(Contact)

admin.site.register(Category)

# admin.site.register(Checkbox)